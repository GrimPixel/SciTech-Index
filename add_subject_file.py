from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")


s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_principal_locale = d_setting["principal locale"]
s_common_text = d_setting["subject header"] + "\n" + s_principal_locale + "\t"

s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)
for p_directory in p_content.rglob("*/"):
    u_directory = PurePath(p_directory)
    s_subject = u_directory.name
    s_text = s_common_text + s_subject + "\n"
    s_file = "• " + s_subject + ".tsv"

    p_file = p_directory.joinpath(s_file)
    if p_file.is_file():
        continue
    p_file.write_text(s_text)
    s_file_path = str(p_file)
    print(s_file_path)

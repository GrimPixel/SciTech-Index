from csv import reader
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")

s_setting_file = "setting.yaml"
p_setting = Path(s_setting_file)
s_setting_text = p_setting.read_text()
d_setting = yaml.load(s_setting_text)

l_locale_selection = d_setting["locale selection"]
e_locale_selection = set(l_locale_selection)

l_subject_selection = d_setting["subject selection"]
e_subject_selection = set(l_subject_selection)

s_extract_file = d_setting["extract file"]
p_extract_file = Path(s_extract_file)

s_extract_header = "\t".join(l_locale_selection)
l_extract_line = [s_extract_header]

s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)

for p_file in p_content.rglob("*.tsv"):
    if l_subject_selection != []:
        s_file_path = str(p_file)
        s_subject_level = s_file_path.removeprefix(s_content_directory)
        l_subject_level_component = s_subject_level.split("/")
        e_subject_level_component = set(l_subject_level_component)
        if e_subject_level_component.intersection(e_subject_selection) == set():
            continue

    d_extracted_component_by_locale = {}
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)

        u_file = PurePath(p_file)
        s_file = u_file.name
        for l_line_component in r_tsv:
            if s_file.startswith("• "):
                s_locale, s_subject = l_line_component
                s_extracted_component = s_subject
            else:
                s_locale, s_concept, s_prerequisite = l_line_component
                s_extracted_component = s_concept

            if s_locale not in l_locale_selection:
                continue
            d_extracted_component_by_locale.update({s_locale: s_extracted_component})
        l_file_locale = d_extracted_component_by_locale.keys()
        e_file_locale = set(l_file_locale)
        if e_file_locale.intersection(e_locale_selection) != e_locale_selection:
            continue

        l_extract_line_component = []
        l_locale_selection_copy = l_locale_selection.copy()
        for s_locale in l_locale_selection:
            l_extract_line_component.append(d_extracted_component_by_locale[s_locale])

        s_extract_line = "\t".join(l_extract_line_component)
        l_extract_line.append(s_extract_line)
        s_extract_file_text = "\n".join(l_extract_line) + "\n"
        p_extract_file.write_text(s_extract_file_text)

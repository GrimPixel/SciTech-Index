from csv import reader
from pathlib import Path, PurePath
from ruamel.yaml import YAML


yaml = YAML(typ="safe")

s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_principal_locale = d_setting["principal locale"]

s_markdown_directory = d_setting["markdown directory"]
p_markdown_directory = Path(s_markdown_directory)
p_markdown_directory.mkdir(exist_ok=True)

s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)
for p_file in p_content.rglob("*.tsv"):
    s_file_path = str(p_file)
    l_markdown_line = []
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        for l_line_component in r_tsv:
            s_markdown_line = "|" + "|".join(l_line_component) + "|"
            l_markdown_line.append(s_markdown_line)

        s_header = l_markdown_line[0]
        i_field_amount = s_header.count("|") - 1
        s_header_end = "|" + "|".join(["-"] * i_field_amount) + "|"
        l_markdown_line.insert(1, s_header_end)
        s_markdown_text = "\n".join(l_markdown_line) + "\n"

        u_file = PurePath(p_file)
        u_file_directory = u_file.parent
        s_file_directory_path = str(u_file_directory)

        s_markdown_file_directory = (
            s_markdown_directory
            + s_file_directory_path.removeprefix(s_content_directory)
        )
        p_markdown_file_directory = Path(s_markdown_file_directory)
        p_markdown_file_directory.mkdir(parents=True, exist_ok=True)

        s_markdown_file = u_file.stem + ".md"
        p_markdown_file = p_markdown_file_directory.joinpath(s_markdown_file)
        u_markdown_file = PurePath(p_markdown_file)

        if p_markdown_file.is_file() and p_markdown_file.read_text() == s_markdown_text:
            continue
        p_markdown_file.write_text(s_markdown_text)

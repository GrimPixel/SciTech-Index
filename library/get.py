def t_get_coupled_locale(t_locale_component):
    s_language, s_script, s_region = t_locale_component
    if s_language in (
        "cdo",
        "cjy",
        "cmn",
        "cpx",
        "cnp",
        "csp",
        "czh",
        "czo",
        "gan",
        "hak",
        "hsn",
        "mnp",
        "nan",
        "wuu",
        "yue",
    ):
        if s_region == "TW" and s_language == "cmn":
            s_coupled_script = "Bopo"
        else:
            s_coupled_script = "Latn"
    elif s_language in (
        "ams",
        "jpn",
        "kzg",
        "mvi",
        "okn",
        "tkn",
        "rvn",
        "ryu",
        "rys",
        "xug",
        "yoi",
        "yox",
    ):
        s_coupled_script = "Hrkt"
    elif s_language in ("jje", "kor"):
        s_coupled_script = "Hang"
    elif s_language in ("vie"):
        s_coupled_script = "Latn"
    else:
        s_coupled_script = ""
    return (s_language, s_coupled_script, s_region)


def l_get_entry_with_sorted_nonlocale(d_nonlocale_by_locale, s_separator):
    l_entry_component = []
    for t_locale_component, s_nonlocale in d_nonlocale_by_locale.items():
        s_language, s_script, s_region = t_locale_component
        t_coupled_locale = t_get_coupled_locale(t_locale_component)
        s_coupled_language, s_coupled_script, s_coupled_region = (
            t_coupled_locale
        )
        if s_coupled_script == "":
            s_locale = "-".join(t_locale_component)
            l_nonlocale_component = s_nonlocale.split(s_separator)
            l_nonlocale_component.sort()
            s_nonlocale = s_separator.join(l_nonlocale_component)
            s_locale_and_nonlocale = (s_locale, s_nonlocale)
            l_entry_component.append(s_locale_and_nonlocale)
        elif s_coupled_script != s_script:
            continue

        l_coupled_locale = [t_locale_component]
        l_nonlocale_component = s_nonlocale.split(s_separator)
        i_nonlocale_component_amount = len(l_nonlocale_component)

        l_coupled_nonlocale_component_and_locale = []
        for i_nonlocale_component_index in range(i_nonlocale_component_amount):
            s_nonlocale_component = l_nonlocale_component[i_nonlocale_component_index]
            l_nonlocale_component_and_locale_component = [
                s_nonlocale_component,
                t_locale_component,
            ]
            l_coupled_nonlocale_component_and_locale.append(
                l_nonlocale_component_and_locale_component
            )

        for (
            t_secondary_locale_component,
            s_secondary_nonlocale,
        ) in d_nonlocale_by_locale.items():
            s_secondary_language, s_secondary_script, s_secondary_region = (
                t_secondary_locale_component
            )
            if not (
                s_secondary_language == s_language
                and s_secondary_script != s_script
                and s_secondary_region == s_region
            ):
                continue
            l_coupled_locale.append(t_secondary_locale_component)
            l_secondary_nonlocale_component = s_secondary_nonlocale.split(s_separator)

            for i_nonlocale_component_index in range(i_nonlocale_component_amount):
                s_secondary_nonlocale_component = l_secondary_nonlocale_component[
                    i_nonlocale_component_index
                ]
                l_coupled_nonlocale_component_and_locale[
                    i_nonlocale_component_index
                ].append(s_secondary_nonlocale_component)
                l_coupled_nonlocale_component_and_locale[
                    i_nonlocale_component_index
                ].append(t_secondary_locale_component)
            l_coupled_nonlocale_component_and_locale.sort()

            for i_nonlocale_component_index in range(i_nonlocale_component_amount):
                d_sorted_nonlocale_by_locale = {}
                for l_name_and_locale in l_coupled_nonlocale_component_and_locale:
                    i_name_and_locale_element_amount = len(l_name_and_locale)
                    for i_name_and_locale_index in range(
                        i_name_and_locale_element_amount
                    ):
                        if i_name_and_locale_index % 2 == 0:
                            continue
                        s_nonlocale_component = l_name_and_locale[
                            i_name_and_locale_index - 1
                        ]
                        t_locale_component = l_name_and_locale[i_name_and_locale_index]
                        s_locale = "-".join(t_locale_component)
                        if s_locale in d_sorted_nonlocale_by_locale.keys():
                            d_sorted_nonlocale_by_locale[s_locale].append(
                                s_nonlocale_component
                            )
                        else:
                            d_sorted_nonlocale_by_locale[s_locale] = [
                                s_nonlocale_component
                            ]

            for s_locale, l_nonlocale_component in d_sorted_nonlocale_by_locale.items():
                s_nonlocale = s_separator.join(l_nonlocale_component)
                t_entry = (s_locale, s_nonlocale)
                l_entry_component.append(t_entry)
    l_entry_component.sort()
    return l_entry_component

from csv import reader
from library.get import l_get_entry_with_sorted_nonlocale
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")

s_setting = "setting.yaml"
p_setting = Path(s_setting)
s_setting_text = p_setting.read_text()
d_setting = yaml.load(s_setting_text)

s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)
for p_file in p_content.rglob("*.tsv"):
    u_file = PurePath(p_file)
    s_file = u_file.name
    if s_file.startswith("• "):
        continue

    s_file_path = str(p_file)
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        s_header = "\t".join(l_header_component)

        d_concept_by_locale = {}
        d_prerequisite_by_locale = {}
        l_line = []
        for l_line_component in r_tsv:
            s_locale, s_concept, s_prerequisite = l_line_component
            l_locale_component = s_locale.split("-")
            t_locale_component = tuple(l_locale_component)
            d_concept_by_locale[t_locale_component] = s_concept
            d_prerequisite_by_locale[t_locale_component] = s_prerequisite
        l_all_locale_and_concept = l_get_entry_with_sorted_nonlocale(
            d_concept_by_locale, " . "
        )
        l_all_locale_and_prerequisite = l_get_entry_with_sorted_nonlocale(
            d_prerequisite_by_locale, " _ "
        )

        i_line_amount = len(l_all_locale_and_concept)
        for i_line_index in range(i_line_amount):
            t_locale_and_concept = l_all_locale_and_concept[i_line_index]
            s_locale, s_concept = t_locale_and_concept
            t_locale_and_prerequisite = l_all_locale_and_prerequisite[i_line_index]
            s_locale, s_prerequisite = t_locale_and_prerequisite
            l_line_component = [s_locale, s_concept, s_prerequisite]
            s_line = "\t".join(l_line_component)
            l_line.append(s_line)
    s_new_text = s_header + "\n" + "\n".join(l_line) + "\n"

    s_text = p_file.read_text()
    if s_new_text == s_text:
        continue
    s_file_path = str(p_file)
    print(s_file_path)
    p_file.write_text(s_new_text)

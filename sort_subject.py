from csv import reader
from library.get import l_get_entry_with_sorted_nonlocale
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")

s_setting = "setting.yaml"
p_setting = Path(s_setting)
s_setting_text = p_setting.read_text()
d_setting = yaml.load(s_setting_text)

s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)
for p_file in p_content.rglob("*.tsv"):
    u_file = PurePath(p_file)
    s_file = u_file.name
    if not s_file.startswith("• "):
        continue

    s_file_path = str(p_file)
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        s_header = "\t".join(l_header_component)

        d_subject_by_locale = {}
        l_line = []
        for l_line_component in r_tsv:
            s_locale, s_subject = l_line_component
            l_locale_component = s_locale.split("-")
            t_locale_component = tuple(l_locale_component)
            d_subject_by_locale[t_locale_component] = s_subject
        l_all_line_component = l_get_entry_with_sorted_nonlocale(
            d_subject_by_locale, " . "
        )

        for t_line_component in l_all_line_component:
            s_line = "\t".join(t_line_component)
            l_line.append(s_line)
    s_new_text = s_header + "\n" + "\n".join(l_line) + "\n"

    s_text = p_file.read_text()
    if s_new_text == s_text:
        continue
    s_file_path = str(p_file)
    print(s_file_path)
    p_file.write_text(s_new_text)
